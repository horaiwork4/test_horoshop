-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: mysql
-- Время создания: Ноя 07 2020 г., 16:24
-- Версия сервера: 5.7.22
-- Версия PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_horoshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `generates`
--

CREATE TABLE `generates` (
  `id` varchar(36) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `generates`
--

INSERT INTO `generates` (`id`, `value`, `created_at`, `updated_at`) VALUES
('6e3eced7-d864-4882-8899-9fb2b666defe', 73467, '2020-11-07 16:23:54', '2020-11-07 16:23:54'),
('c010c4e6-2e99-4ea3-a719-683b83b23a6a', 82392, '2020-11-07 16:24:04', '2020-11-07 16:24:04'),
('e234cc33-adbe-4e27-bc72-f25211a74750', 1234, '2020-11-07 16:02:21', '2020-11-07 16:02:21');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'user123', '$2y$10$bRym9Pw6g/YC1Pz9/Xr2XOb2Ym4CFYki8fv9op0ribsNCjZMrNjBi', '2020-11-07 16:02:21', '2020-11-07 16:02:21');

-- --------------------------------------------------------

--
-- Структура таблицы `user_tokens`
--

CREATE TABLE `user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `token` varchar(32) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `user_id`, `token`, `created_at`, `updated_at`, `expires_at`) VALUES
(4, 1, '094780e18fde8e8b189644b6a5022aa4', '2020-11-07 15:08:53', '2020-11-07 15:08:53', '2020-11-07 15:08:53'),
(5, 1, '00d4c6194d979eabdd42ea867c0cc042', '2020-11-07 15:15:20', '2020-11-07 15:15:20', '2020-11-07 15:15:20'),
(6, 1, '00d4c6194d979eabdd42ea867c0cc042', '2020-11-07 15:15:21', '2020-11-07 15:15:21', '2020-11-07 15:15:21'),
(7, 1, '00d4c6194d979eabdd42ea867c0cc042', '2020-11-07 15:15:22', '2020-11-07 15:15:22', '2020-11-07 15:15:22'),
(8, 1, '00d4c6194d979eabdd42ea867c0cc042', '2020-11-07 15:15:23', '2020-11-07 15:15:23', '2020-11-07 15:15:23'),
(9, 1, '00d4c6194d979eabdd42ea867c0cc042', '2020-11-07 15:15:23', '2020-11-07 15:15:23', '2020-11-07 15:15:23'),
(10, 1, '00d4c6194d979eabdd42ea867c0cc042', '2020-11-07 15:15:23', '2020-11-07 15:15:23', '2020-11-07 15:15:23'),
(11, 1, 'b778d8b009f1de0f73ba2f4567da6530', '2020-11-07 15:16:05', '2020-11-07 15:16:05', '2020-11-07 15:16:05'),
(12, 1, '899092c51bbd82f331d83c029a978819', '2020-11-07 15:16:06', '2020-11-07 15:16:06', '2020-11-07 15:16:06'),
(13, 1, '5033966b8ddcd996b29430c017001eb8', '2020-11-07 15:16:07', '2020-11-07 15:16:07', '2020-11-07 15:16:07'),
(14, 1, '6debc51d3324e88de9d26d39ce4b607c', '2020-11-07 15:17:55', '2020-11-07 15:17:55', '2020-11-07 15:17:55'),
(15, 1, '59b199b9e97cbbd9e5585b6060686aa5', '2020-11-07 15:18:48', '2020-11-07 15:18:48', '2020-11-07 15:18:48'),
(16, 1, 'bb55be28f545f9c580e5661a192b1db3', '2020-11-07 15:20:32', '2020-11-07 15:20:32', '2020-11-07 15:21:32'),
(17, 1, 'a3d2e6598f08db522acf30dc24962b99', '2020-11-07 15:35:52', '2020-11-07 15:35:52', '2020-11-07 15:36:52'),
(18, 1, '6daf76462c205056821be95560ce649e', '2020-11-07 15:35:54', '2020-11-07 15:35:54', '2020-11-07 15:36:54'),
(19, 1, '33315d0c2d92faa7ee249f4ccda59c29', '2020-11-07 15:38:03', '2020-11-07 15:38:03', '2020-11-07 15:39:03'),
(20, 1, '1736382cdac1fc68a2f61f87b2fd17a8', '2020-11-07 15:39:25', '2020-11-07 15:39:25', '2020-11-07 15:40:25'),
(21, 1, 'aa6d8f4ddc598315e26a1de4f09fb93c', '2020-11-07 15:40:10', '2020-11-07 15:40:10', '2020-11-07 15:41:10'),
(22, 1, '43226eceabec5b58eb6be2b1c120ff65', '2020-11-07 15:41:18', '2020-11-07 15:41:18', '2020-11-07 15:42:18'),
(23, 1, '46d38aa5b3bd8639bb740680019c67bb', '2020-11-07 15:54:48', '2020-11-07 15:54:48', '2020-11-07 15:55:48'),
(24, 1, '61c9cbaec7955bb2a1886f987cc62528', '2020-11-07 15:59:22', '2020-11-07 15:59:22', '2020-11-08 18:01:07');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `generates`
--
ALTER TABLE `generates`
  ADD UNIQUE KEY `uuid` (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `user_tokens`
--
ALTER TABLE `user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
