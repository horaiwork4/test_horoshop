<?php

namespace App\Http\Middleware;

use App\Http\Database\DB;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\RedirectResponse;


class AuthMiddleware implements MiddlewareInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $auth = $this->checkAuthorization($request->getHeaders());

        if ($auth === true) {
            return $handler->handle($request);
        }

        // do something with the response
        return new RedirectResponse('/' );
    }

    /**
     * Find a key in an array
     *
     * @param array $request
     * @return void
     */
    private function checkAuthorization($request)
    {
        if (isset($request)) {
            if (array_key_exists('authorization', $request)) {
                return $this->checkTokenValidity(str_replace('Bearer ', '', $request['authorization'][0]));
            }
        }
    }


    /**
     * Token lifetime
     *
     * @param strite $token
     * @return void
     */
    private function checkTokenValidity($token)
    {
        $now = date('Y-m-d H:i:s');

        $sql = "SELECT * FROM `user_tokens` WHERE `token`='$token' AND `expires_at` > '$now'";
        // $stmt = $this->DB()->prepare($sql);
        // $stmt->execute();
        $tokenValid = DB::fetch($sql);


        if($tokenValid) {
            return true;
        }
    }
}
