<?php

namespace App\Http\Controller;

use Carbon\Carbon;
use App\Http\Database\DB;
use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\TextResponse;
use Psr\Http\Message\ServerRequestInterface;

class AuthController
{
    // use ConnectionDB;

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $param = $request->getParsedBody();

        if (isset($param['login']) && isset($param['pass'])) {
            if ($this->validPassword($param['login'], $param['pass'])) {
                // create new token
                $data = $this->createToken($param);
                // update user token
                $this->updateUserToken($data['token']);
                $response = new JsonResponse($data);

                return $response;
            }

            return new TextResponse('Введен неверный пароль.');
        }

        return new TextResponse('Оба поля обязательные.');
    }


    /**
     * Check valid password
     *
     * @param  string $login
     * @param  string $pass
     * @return void
     */
    public function validPassword($login, $pass)
    {
        $sql = "SELECT * FROM users WHERE name = '$login'";
        $user = DB::fetch($sql);

        return password_verify($pass, $user['password']);
    }


    /**
     * Create token
     *
     * @param array $param
     * @return void
     */
    public function createToken($param)
    {
        return ['token'=> md5($param['login'] . $param['pass'] . date('Y-m-d H:i:s'))];
    }


    /**
     * Save new custom token
     *
     * @param string $token
     * @return void
     */
    protected function updateUserToken($token)
    {

        $plus = (new Carbon())->addMinute();
        $now = date('Y-m-d H:i:s');

        $sql = "INSERT INTO user_tokens (`user_id`, `token`,`created_at` , `updated_at` , `expires_at`) VALUES (1, '$token','$now', '$now' , '$plus')";

        DB::insert($sql);
    }
}
