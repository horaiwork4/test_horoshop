<?php

namespace App\Http\Controller;

use Ramsey\Uuid\Uuid;
use App\Http\Database\DB;
use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\TextResponse;
use Psr\Http\Message\ServerRequestInterface;

class GenerateController
{

    public function __invoke(): ResponseInterface
    {
        $data = ['id' => Uuid::uuid4(),'value' => rand(1 , 100000)];
        $this->saveValue($data);

        $response = new JsonResponse($data);

        return $response;
    }


    /**
     * Save the created number
     *
     * @param array $data
     * @return void
     */
    protected function saveValue(array $data)
    {
        $now = date('Y-m-d H:i:s');
        $id = $data['id'];
        $value = $data['value'];

        $sql = "INSERT INTO `generates` (`id`, `value`,`created_at` , `updated_at`) VALUES ('$id', $value,'$now', '$now')";

        DB::insert($sql);
    }

    
}
