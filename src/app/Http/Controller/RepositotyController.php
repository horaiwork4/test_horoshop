<?php
namespace App\Http\Controller;

use App\Http\Database\DB;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\TextResponse;
use Psr\Http\Message\ServerRequestInterface;

class RepositotyController
{

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $param = $request->getQueryParams();

        if (isset($param['id'])) {
            $generate = $this->getValue($param['id']);

            $response = new JsonResponse($generate);

            return $response;
        }
    }


    /**
     * Get value
     *
     * @param int $id
     * @return void
     */
    protected function getValue($id)
    {
        $sql = "SELECT * FROM generates WHERE id = '$id'";

        $generate = DB::fetch($sql);

        return ['value' => $generate['value']];
    }
}
