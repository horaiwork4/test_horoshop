<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\Response\JsonResponse;
use App\Http\Controller\RepositotyController;

class RepositotyControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test__invoke_true_param()
    {
        $request = new ServerRequest;
        
        $repositoty =  $this->getMockBuilder(RepositotyController::class)
                    ->setMethods(['getValue'])
                    ->getMock();


        $repositoty->expects($this->once())
                 ->method('getValue')
                 ->with($this->paramRequest()['id'])
                 ->willReturn(['value' => '123456']);

        $request = $request->withQueryParams($this->paramRequest());

        $response = $repositoty($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
    }


    private function paramRequest()
    {
        return ['id' => 'b35b3184-513a-4087-88bb-78c5aa453676'];
    }
}
