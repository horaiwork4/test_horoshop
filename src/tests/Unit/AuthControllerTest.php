<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Laminas\Diactoros\ServerRequest;
use App\Http\Controller\AuthController;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;


class AuthControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test__invoke_true_param()
    {
        $request = new ServerRequest;

        $auth =  $this->getMockBuilder(AuthController::class)
                    ->setMethods(['validPassword'])
                    ->setMethods(['updateUserToken'])
                    ->getMock();

        $auth->expects($this->once())
                 ->method('validPassword')
                 ->with('user123' , 'qwerty')
                 ->willReturn(true);


        $auth->expects($this->once())
                 ->method('updateUserToken')
                 ->with($auth->createToken($this->paramRequest())['token'])
                 ->willReturn(true);

        $request = $request->withParsedBody($this->paramRequest());

        $response = $auth($request);

        $this->assertInstanceOf(JsonResponse::class , $response);
    }


    private function paramRequest()
    {
        return [
            'login' => 'user123',
            'pass' => 'qwerty'
        ];
    }
}
