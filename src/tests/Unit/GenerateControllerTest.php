<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Controller\GenerateController;
use Laminas\Diactoros\Response\JsonResponse;

class GenerateControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test__invoke_true_param()
    {
        $generate =  $this->getMockBuilder(GenerateController::class)
                    ->setMethods(['saveValue'])
                    ->getMock();

        $generate->expects($this->once())
                 ->method('saveValue')
                 ->willReturn(true);

        $response = $generate();

        $this->assertInstanceOf(JsonResponse::class, $response);
    }


}
